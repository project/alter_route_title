# Alter Route Title

This module will help you to alter existing router titles,
the module only processing the contributed modules and custom
modules also you can also you can still use _title_callback
we are not hammering the drupal in-built title callback
functioality for dynamic title. Also you can export/import
Alter Route Title configurations.

## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Goto Administration >> Configuration >> System >> Alter Route Title
1. You can see the available list of routes and title,
   in the same table you add your custom titles.

## Maintainers

- Hari Venu - [harivenuv](https://www.drupal.org/u/harivenuv)
